const express = require('express');
//Importar serverController
const ServerController = require('../controllers/serverController');

class ServerRouter{
    constructor(){
        this.router = express.Router();
        this.config();
    }

    config(){
        const objServerC = new ServerController.default();
        this.router.get("/users", objServerC.getAllUsers);
        this.router.get("/users/:id", objServerC.getUserById);
        this.router.post("/users", objServerC.register);
        this.router.delete("/users", objServerC.deleteUser);
        this.router.put("/users", objServerC.updateUser);
    }
}

exports.default = ServerRouter; 