const personas = require('../models/person');

var users = [
    { id: 1, name: "Alexander", lastname: "Ledezma" },
    { id: 2, name: "Erika", lastname: "Castro" },
    { id: 3, name: "Andrés", lastname: "Quintero" },
    { id: 4, name: "Luisa", lastname: "Carmona" },
    { id: 5, name: "Esteban", lastname: "Carranza" }
];


class ServerController {
    constructor() {

    }

    register(req, res) {
        //crear un usuario en la BD
        personas.create(req.body, (error, data) => {
            if (error) {
                res.status(500).json({ message: "error" });
            } else {
                res.status(201).json({ message: "Usuario creado" });
                //res.status(201).json(data);
            }
        });

    }

    /*
    register(req, res){
        let { id, name, lastname } = req.body;
        console.table({id, name, lastname});
        //añadir el usuario al array
        users.push(req.body);
        res.status(201).json({message: "Usuario creado"});
    }
    */
    getAllUsers(req, res) {
        personas.find((error, data) => {
            if (error) {
                res.status(500).json(error);
            } else {
                res.status(200).json(data);
            }
        });
    }
    /*
    getAllUsers(req, res){
        console.log("Petición exitosa");
        res.status(200).json(users);
    }
    */

    getUserById(req, res) {
        let id = req.params.id;
        personas.findById(id, (error, data) => {
            if (error) {
                res.status(401).json({ message: "error" })
            } else {
                res.status(200).json(data);
            }
        });
    }

    /*
    getUserById(req, res){
        let id = req.params.id;
        let tempoUser = null;
        users.forEach(element => {
            if(id == element.id){
                tempoUser = element;
            }
        });

        if(tempoUser != null){
            res.status(200).json(tempoUser);
        }else{
            res.status(404).send();
        }
        
    }
    */

    deleteUser(req, res) {
        let { id } = req.body;
        personas.findByIdAndRemove(id, (error, data) => {
            if (error) {
                res.status(500).json({ message: "Error" });
            } else {
                res.status(200).json(data);
            }
        })
    }

    updateUser(req, res) {
        let { id } = req.body;
        let { name, lastname, phone, email, age } = req.body;
        let obj = {
            name,
            lastname,
            phone,
            email,
            age
        }
        console.log(obj);
        personas.findByIdAndUpdate(id, {
            $set: obj
        }, (error, data) => {
            if (error) {
                res.status(500).json({ message: "error" });
            } else {
                res.status(200).json(data);
            }
        })
    }

}

exports.default = ServerController;