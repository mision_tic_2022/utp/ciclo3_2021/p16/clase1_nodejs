//Importar express
const express = require('express');
//Importar serverRouter
const serverRouter = require('./routers/serverRouter');
//Importar mongoose
const mongoose = require('mongoose');
//Importar url de conexión a la BD
const database = require('./database/db');

class Server {
    //Constructor
    constructor() {
        //Conectar a la bd
        this.conectar_bd();
        //Creamos un objeto express
        this.app = express();
        //Indicar el puerto por el que se montará el servidor
        this.app.set('port', process.env.PORT || 3000);
        //Indicar que se manejará solicitudes en formato Json
        this.app.use(express.json());
        //Crear ruta de inicio
        const router = express.Router();
        router.get('/', (req, res) => {
            res.status(200).json({
                message: "Conexión exitosa"
            });
        });
        //Añadir la ruta al servidor
        const objServerR = new serverRouter.default();
        this.app.use(objServerR.router);
        //Añadir la ruta creada al servidor
        this.app.use(router);
        //Levantar el servidor / poner el servidor a la escucha
        this.app.listen(this.app.get('port'), () => {
            console.log("Servidor corriendo sobre el puerto => ", this.app.get('port'));
        });
    }

    conectar_bd() {
        //mongoose.Promise = global.Promise;
        mongoose.connect(database.db).then(() => {
            console.log("Conexión exitosa a  mongodb");
        }, (error) => {
            console.log(error);
        }).catch((error)=>{
            console.log(error);
        });
    }
}

const objServer = new Server();

