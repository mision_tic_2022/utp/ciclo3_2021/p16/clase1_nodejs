const mongoose = require('mongoose');

const schema = mongoose.Schema;

var personSchema = new schema({
    name: {
        type: String
    },
    lastname: {
        type: String
    },
    phone: {
        type: String
    },
    email: {
        type: String
    },
    age: {
        type: Number
    }
}, 
    {collection: 'personas'}
);

module.exports = mongoose.model("Person", personSchema);